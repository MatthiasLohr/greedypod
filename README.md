# GreedyPod

*A resource hungry, traffic producing, greedy Kubernetes Pod. Nothing else.*

## Deployment

Basically, you have three choices for deployment:

  * manually [download](/../builds/artifacts/master/raw/greedypod?job=build) and execute the binary:
    ```
    chmod a+x greedypod
    ./greedypod
    ```
  * download and run it via docker:
    ```
    docker run -d -p 8080:8080 registry.gitlab.com/matthiaslohr/greedypod
    ```
  * deploy in your Kubernetes cluster using the Helm Chart:
    ```
    helm repo add mlohr https://helm-charts.mlohr.com/
    helm repo update
    helm install greedypod deployments/helm-chart \
      --set ingress.hostname=greedypod.example.com
    ```
    For more configuration parameters, see [Helm Chart Configuration Parameters](#helm-chart-configuration-parameters).

## Usage

GreedyPod is also a lazy Pod. It idles and waits for your initiative.
It's also stupid, it only understands three words:

  * `l`: Waste `l` milliseconds of time with doing nothing.
  * `t`: Waste `t` milliseconds of CPU time.
  * `s`: Waste `s` bytes of traffic.
  
Example call via curl:
```
curl -v -o /dev/null http://localhost:8080/?l=1000&t=1000&s=1000
```

It does not much. But it does it very well.


## Helm Chart Configuration Parameters

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `deployment.replicas` | Number of GreedyPods to be started | `8` |
| `deployment.image` | Image to use for the deployment | `registry.gitlab.com/matthiaslohr/greedypod:latest` |
| `deployment.imagePullPolicy` | ImagePullPolicy | `Always` |
| `service.type` | The type of the service to be created (`ClusterIP`, `NodePort` or `LoadBalancer` | `ClusterIP` |
| `service.loadBalancerIP` | IP of the service when type is `LoadBalancer` | `null` |
| `ingress.enabled` | Should an Ingress object be created | `true` |
| `ingress.hostname` | Hostname for reaching your GreedyPods | `null` |
| `ingress.path` | Path for reaching your GreedyPods | `null` |
| `ingree.tls.enabled` | Enable SSL for Ingress | `true` |


## License

*MIT License*

```
Copyright (c) 2020 Matthias Lohr <mail@mlohr.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```