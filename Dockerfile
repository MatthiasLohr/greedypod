
FROM scratch
COPY ./greedypod /opt/greedypod
EXPOSE 8080/tcp
ENTRYPOINT ["/opt/greedypod"]
