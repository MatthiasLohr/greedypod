package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func greedyRequest(w http.ResponseWriter, req *http.Request) {
	// assign default value
	latency := 0
	cpuTime := 0
	var dataSize int64 = 0

	if len(req.URL.Query()["l"]) > 0 {
		tmpLatency, err := strconv.Atoi(req.URL.Query()["l"][0])
		if err != nil {
			log.Println("Could not parse latency")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		latency = tmpLatency
	}

	if len(req.URL.Query()["t"]) > 0 {
		tmpCpuTime, err := strconv.Atoi(req.URL.Query()["t"][0])
		if err != nil {
			log.Println("Could not parse CPU time")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		cpuTime = tmpCpuTime
	}

	if len(req.URL.Query()["s"]) > 0 {
		tmpDataSize, err := strconv.ParseInt(req.URL.Query()["s"][0], 10, 64)
		if err != nil {
			log.Println("Could not parse data size")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		dataSize = tmpDataSize
	}

	log.Printf("After latency of %d ms, work %d ms and send back %d bytes", latency, cpuTime, dataSize)
	time.Sleep(time.Duration(latency) * time.Millisecond)
	deadline := time.Now().Add(time.Duration(cpuTime) * time.Millisecond)
	for time.Now().Before(deadline) {
		// waste cpu
	}
	fdZero, err := os.Open("/dev/zero")
	if err != nil {
		panic(err)
	}
	defer fdZero.Close()
	io.CopyN(w, fdZero, dataSize)
}

func main()  {
	http.HandleFunc("/", greedyRequest)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Panic(err)
	}
}
